package com.translate.controller;

import com.translate.common.AjaxResult;
import com.translate.common.RequestEntity;
import com.translate.service.TranslateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author ZXINHAO
 * @date 2023/3/15
 */
@RestController
@RequestMapping("/trans")
public class TranslateController {
    @Autowired
    private TranslateService translateService;

    @PostMapping
    public AjaxResult transContent(@RequestBody RequestEntity entity){
        return translateService.transContent(entity.getQuery(),entity.getType());
    }
}
