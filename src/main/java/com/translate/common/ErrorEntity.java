package com.translate.common;

import lombok.Data;

@Data
public class ErrorEntity {
    private String error_code;
    private String error_msg;
}
