package com.translate.common;

public enum ErrorEnum {
    // 数据操作错误定义
    SUCCESS(52000, "成功"),
    REQUEST_TIMEOUT(52001,"请求超时"),
    SYSTEM_ERROR(52002,"系统错误"),
    UNAUTHORIZED(52003,"未授权用户"),
    PARAMETER_ISNULL(54000,"必填参数为空"),
    SIGN_ERROR(54001,"签名错误"),
    ACCESS_FREQUENCY(54003,"访问频率受限"),
    INSUFFCIENT_ACCOUNT_BALANCE(54004,"账户余额不足"),
    LONG_QUERY(54005,"长query请求频繁"),
    CLIENT_IP_ILLEGAL(58000,"客户端IP非法"),
    TRANSLATED_TEXT(58001,"译文语言方向不支持"),
    SERVICE_CLOSE(58002,"服务当前已关闭"),
    ATTESTATION_NULL(90107,"认证未通过或未生效"),
    PARSE_ERROR(500,"内容解析失败")
    ;

    /** 错误码 */
    private Integer errorCode;

    /** 错误信息 */
    private String errorMsg;

    ErrorEnum(Integer errorCode, String errorMsg) {
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
    }

    public Integer getErrorCode() {
        return errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }
}
