package com.translate.common;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
/**
 * 请求实体类
 */
public class TranslateEntity {
    private String q; //内容
    private String from;//翻译源语言
    private String to;//译文语言
    private String appid; //百度appid
    private String salt;//随机码
    private String sign;// appid+q+salt+密钥 的MD5值
}
