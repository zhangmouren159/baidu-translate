package com.translate.common;

import lombok.Data;

/**
 * @author ZXINHAO
 * @date 2023/3/15
 */
@Data
public class RequestEntity {
    private String query;
    private String type;
}
