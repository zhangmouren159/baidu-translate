package com.translate.common;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


@NoArgsConstructor
@Data
public class SuccessEntity {


    /**
     * {"from":"zh","to":"en","trans_result":[{"src":"中国","dst":"China"}]}
     */


    @JsonProperty("from")
    private String from;
    @JsonProperty("to")
    private String to;
    @JsonProperty("trans_result")
    private List<TransResultDTO> transResult;

    @NoArgsConstructor
    @Data
    public static class TransResultDTO {
        @JsonProperty("src")
        private String src;
        @JsonProperty("dst")
        private String dst;
    }
}
