# 论文去重Java版()

#### 介绍

基于百度翻译API

通过不断地在多种语言之间翻译，最终翻回中文。
由于各国语言习惯不同，最终翻译出来的文字，在进行简单调整之后，就达到去重的效果。


#### 软件架构

软件架构说明

基于springboot+vue


#### 使用说明

1.需要去百度翻译开放平台申请账号,然后使用appid,secretKey  配置到 application.yml 文件中
2.页面在 src-main-html 目录下

原版C端地址[52破解](https://www.52pojie.cn/thread-1152034-1-1.html)

![界面](src/main/resources/%E5%9B%BE%E7%89%871.jpg)

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
